package com.muraliveeravalli.springhibernated.service;

import com.muraliveeravalli.springhibernated.dao.CustomerDao;
import com.muraliveeravalli.springhibernated.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Override
    @Transactional
    public List<Customer> getCustomers() {
        return customerDao.getCustomers();
    }

    @Override
    @Transactional
    public void saveCustomer(Customer customer) {
        customerDao.SaveCustomer(customer);

    }

    @Override
    @Transactional
    public Customer getCustomerForUpdate(int id) {
        return customerDao.getCustomerForUpdate(id);
    }

    @Override
    @Transactional
    public void deleteCustomer(int id) {
        customerDao.deleteCustomer(id);
    }
}
