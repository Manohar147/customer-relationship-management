package com.muraliveeravalli.springhibernated.service;

import com.muraliveeravalli.springhibernated.entity.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getCustomers();

    void saveCustomer(Customer customer);

    Customer getCustomerForUpdate(int id);

    void deleteCustomer(int id);
}
