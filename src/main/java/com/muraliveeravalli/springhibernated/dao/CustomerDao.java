package com.muraliveeravalli.springhibernated.dao;

import com.muraliveeravalli.springhibernated.entity.Customer;

import java.util.List;

public interface CustomerDao {

    List<Customer> getCustomers();

    void SaveCustomer(Customer customer);

    Customer getCustomerForUpdate(int id);

    void deleteCustomer(int id);
}
