package com.muraliveeravalli.springhibernated.dao;

import com.muraliveeravalli.springhibernated.entity.Customer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerDaoImpl implements CustomerDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Customer> getCustomers() {
        return sessionFactory.getCurrentSession().createQuery("from Customer order by lastName", Customer.class).getResultList();
    }

    @Override
    public void SaveCustomer(Customer customer) {
        sessionFactory.getCurrentSession().saveOrUpdate(customer);
    }

    @Override
    public Customer getCustomerForUpdate(int id) {
        return sessionFactory.getCurrentSession().get(Customer.class, id);
    }

    @Override
    public void deleteCustomer(int id) {
        sessionFactory.getCurrentSession()
                .createQuery("DELETE from Customer where id=:customerId")
                .setParameter("customerId", id)
                .executeUpdate();
    }
}
