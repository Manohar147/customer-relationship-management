package com.muraliveeravalli.springhibernated.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Aspect
@Component
public class CRMLoggingAspect {
    private static final Logger LOGGER = Logger.getLogger(CRMLoggingAspect.class.getName());

    @Pointcut("execution(* com.muraliveeravalli.springhibernated.controller.*.*(..))")
    public void forControllerPackage() {

    }

    @Pointcut("execution(* com.muraliveeravalli.springhibernated.service.*.*(..))")
    public void forServicePackage() {

    }

    @Pointcut("execution(* com.muraliveeravalli.springhibernated.dao.*.*(..))")
    public void forDaoPackage() {

    }

    @Pointcut("forControllerPackage() || forServicePackage() || forDaoPackage()")
    public void appFlow() {

    }

    @Before("appFlow()")
    public void before(JoinPoint joinPoint) {
        LOGGER.info("In @Before Method: " + joinPoint.getSignature().toShortString());
        LOGGER.info("Args: " + Arrays.stream(joinPoint.getArgs()).filter(Objects::nonNull).map(Object::toString).collect(Collectors.joining(",")));
    }

    @AfterReturning(pointcut = "appFlow()", returning = "result")
    public void afterReturning(JoinPoint joinPoint, Object result) {
        LOGGER.info("In @AfterReturning Method: " + joinPoint.getSignature().toShortString());
        LOGGER.info("@returning: " + result);
    }

}
